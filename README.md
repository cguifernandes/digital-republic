# Meu Projeto React

Bem-vindo ao meu projeto React! Este é um repositório onde você pode encontrar o código-fonte de um projeto React.

## Como Começar

Siga estas instruções para configurar e executar o projeto localmente em sua máquina.

### Pré-requisitos

Certifique-se de ter o seguinte instalado em sua máquina:

- Node.js
- npm (geralmente vem com o Node.js)

### Clonar o Repositório

```bash
git clone https://gitlab.com/cguifernandes/digital-republic.git
```

### Entrar na pasta
```bash
cd digital-republic
```

### Instalar dependências
```bash
npm install
```

### Rodar projeto
```bash
npm run start
```