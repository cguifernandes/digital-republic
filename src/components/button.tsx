import { type ReactNode, forwardRef, type ButtonHTMLAttributes } from "react";
import { cn } from "../lib/utils";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
	className?: string;
	children: ReactNode;
}

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
	({ className, children, ...props }, ref) => {
		return (
			<button
				className={cn(
					"flex items-center h-10 px-4 py-2 justify-center rounded-md text-sm font-medium focus-visible:ring-2 bg-slate-900 text-slate-50 hover:bg-slate-900/90",
					className,
				)}
				ref={ref}
				{...props}
			>
				{children}
			</button>
		);
	},
);

export default Button;
