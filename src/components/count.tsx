import { useState } from "react";
import type { UseFormSetValue } from "react-hook-form";
import Button from "./button";

interface Props {
	label?: string;
	// biome-ignore lint/suspicious/noExplicitAny: <explanation>
	setValue?: UseFormSetValue<any>;
	name?: string;
}

const Count = ({ label, setValue, name }: Props) => {
	const [count, setCount] = useState(0);

	if (label) {
		return (
			<div className="flex flex-col gap-y-1">
				<label className="text-center text-slate-900 text-sm">{label}</label>
				<div className="flex gap-x-6 items-center">
					<Button
						type="button"
						onClick={() => {
							setCount((prev) => {
								const newCount = prev > 0 ? prev - 1 : prev;
								if (setValue && name) {
									setValue(name, newCount);
								}

								return newCount;
							});
						}}
					>
						-
					</Button>
					<span className="w-3 flex items-center justify-center">{count}</span>
					<Button
						type="button"
						onClick={() => {
							setCount((prev) => {
								const newCount = prev + 1;
								if (setValue && name) {
									setValue(name, newCount);
								}

								return newCount;
							});
						}}
					>
						+
					</Button>
				</div>
			</div>
		);
	}

	return (
		<div className="flex gap-x-6 items-center">
			<Button
				onClick={() => {
					setCount((prev) => {
						const newCount = prev > 0 ? prev - 1 : prev;
						if (setValue && name) {
							setValue(name, newCount);
						}

						return newCount;
					});
				}}
			>
				-
			</Button>
			<span className="w-3 flex items-center justify-center">{count}</span>
			<Button
				onClick={() => {
					setCount((prev) => {
						const newCount = prev + 1;
						if (setValue && name) {
							setValue(name, newCount);
						}

						return newCount;
					});
				}}
			>
				+
			</Button>
		</div>
	);
};

export default Count;
