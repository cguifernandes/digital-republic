import { useFieldArray, useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import Count from "../count";
import Button from "../button";
import Input from "../input";

const FormPaint = () => {
	const wallSchema = z
		.object({
			width: z.number().nullable(),
			height: z.number().nullable(),
			windows: z.number().nonnegative(),
			doors: z.number().nonnegative(),
		})
		.transform((data) => {
			return {
				...data,
				width: data.width !== null ? data.width / 100 : null,
				height: data.height !== null ? data.height / 100 : null,
			};
		})
		.refine(
			(data) => {
				const totalArea = (data.width ?? 0) * (data.height ?? 0);
				return totalArea >= 1 && totalArea <= 50;
			},
			{
				message: "A área da parede deve ser entre 1 e 50 metros quadrados",
				path: ["width"],
			},
		)
		.refine(
			(data) => {
				const doorArea = (data.doors ?? 0) * 0.8 * 1.9;
				const windowArea = (data.windows ?? 0) * 2.0 * 1.2;
				const wallArea = (data.width ?? 0) * (data.height ?? 0);
				return doorArea + windowArea <= wallArea * 0.5;
			},
			{
				message:
					"A área das portas e janelas não pode exceder 50% da área da parede",
				path: ["width"],
			},
		)
		.refine(
			(data) => {
				if ((data.doors ?? 0) > 0) {
					return (data.height ?? 0) >= 2.2;
				}
				return true;
			},
			{
				message:
					"A altura da parede deve ser pelo menos 30cm maior que a altura da porta",
				path: ["height"],
			},
		);

	const roomSchema = z.object({
		walls: z.array(wallSchema).length(4),
	});

	const form = useForm<z.infer<typeof roomSchema>>({
		resolver: zodResolver(roomSchema),
		reValidateMode: "onSubmit",
		defaultValues: {
			walls: [
				{ width: null, height: null, windows: 0, doors: 0 },
				{ width: null, height: null, windows: 0, doors: 0 },
				{ width: null, height: null, windows: 0, doors: 0 },
				{ width: null, height: null, windows: 0, doors: 0 },
			],
		},
	});

	const { fields } = useFieldArray({
		control: form.control,
		name: "walls",
	});

	const handlerSubmitForm = (data: z.infer<typeof roomSchema>) => {
		const paintArea = data.walls.reduce((acc, wall) => {
			const width = wall.width ?? 0;
			const height = wall.height ?? 0;

			const wallArea = width * height;
			const doorArea = (wall.doors ?? 0) * 0.8 * 1.9;
			const windowArea = (wall.windows ?? 0) * 2.0 * 1.2;
			return acc + (wallArea - doorArea - windowArea);
		}, 0);

		let liters = paintArea / 5;
		const cans = [18, 3.6, 2.5, 0.5];
		const count: { [key: number]: number } = {};

		for (const size of cans) {
			count[size] = Math.floor(liters / size);
			liters %= size;
		}

		console.log(paintArea / 5);

		if (liters > 0) {
			count[0.5] = (count[0.5] || 0) + 1;
		}

		const formatCount = Object.entries(count)
			.filter(([_, quantity]) => quantity > 0)
			.map(([size, quantity]) => `${quantity} lata(s) de ${size} litros`)
			.join(", ");

		alert(formatCount);
	};

	return (
		<form
			onSubmit={form.handleSubmit(handlerSubmitForm)}
			className="max-w-5xl mx-auto w-full flex flex-wrap gap-4 items-center px-6"
		>
			{fields.map((_, index) => {
				const error = form.formState.errors.walls?.[index];

				return (
					<div
						// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
						key={index}
						className="flex flex-col max-w-[calc(50%-1rem)] space-y-3 w-full"
					>
						<label className="text-slate-900 text-sm">Parede {index + 1}</label>
						<div className="flex flex-col !mt-1 gap-y-1">
							<Input
								maxLength={50}
								placeholder={`Largura parede ${index + 1} (em cm)`}
								{...form.register(`walls.${index}.width`, {
									valueAsNumber: true,
								})}
							/>
							{error?.width && (
								<p className="text-sm font-medium text-red-500">
									{error?.width.message}
								</p>
							)}
						</div>
						<div className="flex flex-col gap-y-1">
							<Input
								maxLength={50}
								placeholder={`Altura parede ${index + 1} (em cm)`}
								{...form.register(`walls.${index}.height`, {
									valueAsNumber: true,
								})}
							/>
							{error?.height && (
								<p className="text-sm font-medium text-red-500">
									{error?.height.message}
								</p>
							)}
						</div>
						<div className="flex gap-x-4 justify-evenly">
							<Count
								setValue={form.setValue}
								name={`walls.${index}.doors`}
								label="Portas (0,80 x 1,90)"
							/>
							<Count
								setValue={form.setValue}
								name={`walls.${index}.windows`}
								label="Janelas (2,00 x 1,20)"
							/>
						</div>
					</div>
				);
			})}
			<Button className="w-2/3 mt-2 mx-auto" type="submit">
				Enviar
			</Button>
		</form>
	);
};

export default FormPaint;
