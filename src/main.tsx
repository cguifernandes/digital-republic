import FormPaint from "./components/Form/form-paint";

const Main = () => {
	return (
		<main className="min-h-screen flex items-center justify-center">
			<FormPaint />
		</main>
	);
};

export default Main;
